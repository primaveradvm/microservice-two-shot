from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from common.json import ModelEncoder
from .models import BinVO, Shoe

# Create your views here.
class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "name",
        "id",
        ]

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "import_href",
        "bin_size",
        "bin_number",
        "shoe",
    ]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "name",
        "manufacturer",
        "color",
        "url",
        "id",
        ]
    def get_extra_data(self, o):
        return {"bin": o.bin.id}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET": #GET getting a list of all shoes
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else: #use POST method to create SHOE
        content = json.loads(request.body)

        try:
            bin = BinVO.objects.get(import_href=content["bin"])
            content["bin"]=bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "invalid bin id"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoe(request, pk):
     if request.method == "GET": #GET show shoe details
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
     elif request.method == "DELETE": #deleting a shoe
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
     else: #updating a shoe with PUT
        content = json.loads(request.body)
        try:
            if "bin" in content:
                bin = BinVO.objects.get(abbreviation=content["bin"])
                content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin"},
                status=400,
            )
        Shoe.objects.filter(id=pk).update(**content)
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

def api_list_bins(request):
    if request.method =="GET":
        bins = BinVO.objects.all()
        return JsonResponse(
            bins,
            encoder=BinVOEncoder,
            safe=False
        )
