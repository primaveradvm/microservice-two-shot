from django.db import models
from django.urls import reverse

# # Create your models here.
# The Shoe resource should track its manufacturer,
# its model name, its color, a URL for a picture,
# and the bin in the wardrobe where it exists.

class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)
    bin_number = models.IntegerField()
    bin_size = models.IntegerField()


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=40)
    name = models.CharField(max_length=80)
    color = models.CharField(max_length=40)
    url = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes", #related name: needed!
        on_delete=models.PROTECT,
    )

    def get_api_url(self):
        return reverse("api_show_shoe", kwargs={"pk": self.pk})

    def __str__(self):
        return self.name
