import React, {useState, useEffect } from 'react';

function ShoeForm() {
  const [bins, setBins] = useState([])
  const [formData, setFormData] = useState({
    name: '',
    manufacturer: '',
    bin: '',
	  color: '',
	  url:  ''
  })
  console.log("formData", formData)

  const [hasSignedUp, setHasSignedUp] = useState(false)

  const getData = async () => {
    const url = 'http://localhost:8100/api/bins/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setBins(data.bins);
    }
  }

  useEffect(() => {
    getData();
  }, []);
// what to do after this??

// const addShoe = (shoe) => {
//     const newShoe = []
// }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const shoeUrl = `http://localhost:8080/api/shoes/`;
    console.log("shoeUrl"+ shoeUrl)

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(shoeUrl, fetchConfig);
    console.log("response" , response)

    if (response.ok) {
      setFormData({
        name: '',
        manufacturer: '',
        bin: '',
        color: '',
        url: ''
      });

      setHasSignedUp(true);
    }
  }

  const handleChangeName = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value
    });
  }

  //Allows for the form to display on when a user has not signed in.
  //If a user has signed in, the css class d-none is added to the unneeded piece of the interface

  const formClasses = (!hasSignedUp) ? '' : 'd-none';
  const messageClasses = (!hasSignedUp) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';

  return (
    <div className="my-5">
      <div className="row">
        <div className="col col-sm-auto">
          <img
            width="300"
            className="bg-white rounded shadow d-block mx-auto mb-4"
            src="logo.svg"
            alt='Logo'
          />
        </div>

        <div className="col">
          <div className="card shadow">
            <div className="card-body">

              <form className={formClasses} onSubmit={handleSubmit} id="create-attendee-form">
                <h1 className="card-title">create a shoe</h1>
                <p className="mb-3">
                  please create shoe details
                </p>

                <div className="mb-3">
                  <select onChange={handleChangeName} name="bin" id="bin" required>
                    <option value="">select bin</option>
                    {
                      bins.map(bin => {
                        return (
                          <option key={bin.href} value={bin.href}>{bin.id}</option>
                        )
                      })
                    }
                  </select>
                </div>

                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} required placeholder="name" type="text" id="name" name="name" className="form-control" />
                      <label htmlFor="name">name your shoe</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} required placeholder="manufacturer" type="text" id="manufacturer" name="manufacturer" className="form-control" />
                      <label htmlFor="manufacturer">which company will manufacture your shoes in a 3rd world country?</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} required placeholder="color" type="text" id="color" name="color" className="form-control" />
                      <label htmlFor="color">color</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value = {formData.url} required placeholder="url" type="text" id="url" name="url" className="form-control" />
                      <label htmlFor="url">url</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">create a shoe</button>
              </form>

              <div className={messageClasses} id="success-message">
                good job, you made a shoe, now go do real work
                <button onClick ={ () => setHasSignedUp(false)}>
                  make another shoe
                </button>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ShoeForm;
