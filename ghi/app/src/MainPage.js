import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function ShoeColumn(props) {
  console.log(props)
  return (
    <div className="col">
    {props.list.map(shoe => {
      console.log(shoe)
      return (
        <div key={shoe.id} className="card mb-3 shadow">
          <img
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRgR4MSgOfQ_Pqbx4wjpLmS3tuIPk8iXougRtxBd_sEKEl190HnroZWdMN8kluTCXmajrE&usqp=CAU"
            // src={conference.location.picture_url}
            className="card-img-top"
            alt='shoe mofo'
          />
          <div className="card-body">
            <h5 className="card-title">{shoe.name}</h5>
            <h6 className="card-subtitle mb-2 text-muted">
              {shoe.manufacturer}

            </h6>
            <p className="card-text">
              {shoe.color}
              {shoe.url}
            </p>
          </div>
          <div className="card-footer">
            bin # {shoe.bin}
          </div>
        </div>
      );
    })}
    </div>
    );
  }


function MainPage(props) {
  const [shoeColumns, setShoeColumns] = useState([[], [], []]);

  async function getShoes(){
    const url = 'http://localhost:8080/api/shoes/';

    try {
      const response = await fetch(url);
      if (response.ok) {
        console.log("response OK");
        const data = await response.json();

        const requests = [];
        for (let shoe of data.shoes) {
          const detailUrl = `http://localhost:8080/api/shoes/${shoe.id}/`;
          console.log("got shoe detailUrl")
          requests.push(fetch(detailUrl));
        }

        const responses = await Promise.all(requests);
        const shoeColumns = [[], [], []];


        let i = 0;
        for (const shoeResponse of responses) {
          if (shoeResponse.ok) {
            const details = await shoeResponse.json();
            shoeColumns[i].push(details);
            i = i + 1;
            if (i > 2) {
              i = 0;
        }
      } else {
        console.error(shoeResponse);
        console.log("error, shoeResponse NOT ok")
      }
    }
    setShoeColumns(shoeColumns);
    }
  } catch(e) {
    console.error(e);
      }
    }

    ////
    const deliciousLunchThings = [
      {person: 'Me', item: 'food'},
      {person: 'You', item: 'frozen pickled tomato juice'}
    ]

  useEffect(() => {
    getShoes();
  }, []);

  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          Need to keep track of your shoes and hats? We have
          the solution for you!
        </p>
      </div>
      <div className="container">
        <h2>gimme your shoes fool</h2>
        <div className="row">
          {shoeColumns.map((shoeList, index) => {
            return (
              <ShoeColumn key={index} list={shoeList} />
            );
          })}
        </div>
      </div>

    </div>
  );
}

export default MainPage;
