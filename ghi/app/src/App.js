import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';
import Testing from './Testing';


function App() {

  return (
    <>
    <div className="App">
        <h1>testing</h1>
    </div>
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" element={<ShoeList />} />
          <Route path="/shoes/create" element={<ShoeForm />} />
          {/* <Route path="/testing" element={<Testing />} /> */}
        </Routes>
      </div>
    </BrowserRouter>
    </>
  );

}

export default App;
